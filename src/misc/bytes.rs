pub trait Bytes {
	fn split_into(self, parts: Vec<&mut Vec<u8>>, by: u8);
	fn split_by(self, by: u8) -> Vec<Vec<u8>>;
}

impl Bytes for Vec<u8> {
	fn split_into(self, mut parts: Vec<&mut Vec<u8>>, by: u8) {
		let mut i = 0_usize;
		for byte in self {
			if byte == by {
				i += 1;
				continue;
			} match parts.get_mut(i) {
				Some(some) => some.push(byte),
				None => break
			};
		}
	}

	fn split_by(self, by: u8) -> Vec<Vec<u8>> {
		let mut out: Vec<Vec<u8>> = Vec::new();
		out.push(Vec::new());

		for byte in self {
			if byte == by {
				out.push(Vec::new());
				continue;
			} out.last_mut().unwrap().push(byte);
		} out
	}
}
