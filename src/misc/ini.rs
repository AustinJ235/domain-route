use std::collections::HashMap;
use std::fs::File;
use std::io::{Read,Write};
use misc::bytes::Bytes;

pub struct Ini {
	map: HashMap<String, String>
}

fn encode(bytes: Vec<u8>) -> Vec<u8> {
	let mut out = Vec::new();
	for byte in bytes {
		if byte == 92 {	
			out.push(92);
			out.push(byte);
		} else if byte == 0 {
			out.push(92);
			out.push(48);
		} else if byte == 7 {
			out.push(92);
			out.push(97);
		} else if byte == 8 {
			out.push(92);
			out.push(98);
		} else if byte == 9 {
			out.push(92);
			out.push(116);
		} else if byte == 13 {
			out.push(92);
			out.push(114);
		} else if byte == 10 {
			out.push(92);
			out.push(110);
		} else if byte == 59 {
			out.push(92);
			out.push(byte);
		} else if byte == 35 {
			out.push(92);
			out.push(byte);
		} else if byte == 61 {
			out.push(92);
			out.push(byte);
		} else if byte == 58 {
			out.push(92);
			out.push(byte);
		} else if byte == 32 {
			out.push(92);
			out.push(115);
		} else {
			out.push(byte)
		}
	} out
}

fn decode(bytes: Vec<u8>) -> Vec<u8> {
	let mut out = Vec::new();
	let mut iter = bytes.into_iter();
	loop {
		match iter.next() {
			Some(some) => {
				if some == 92 {
					match iter.next() {
						Some(some) => {
							match some {
								92 => out.push(some),
								48 => out.push(0),
								97 => out.push(7),
								98 => out.push(8),
								116 => out.push(9),
								114 => out.push(13),
								110 => out.push(10),
								59 => out.push(some),
								35 => out.push(some),
								61 => out.push(some),
								58 => out.push(some),
								115 => out.push(32),
								_ => out.push(some)
							}
						}, None => break
					}
				} else {
					out.push(some)
				}
			}, None => break
		}
	} out
}

impl Ini {
	pub fn new() -> Self {
		Ini {
			map: HashMap::new()
		}
	}

	pub fn load(path: String) -> Result<Self, String> {
		let mut handle = match File::open(path.as_str()) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		}; let mut buffer = Vec::new();
		
		if let Err(e) = handle.read_to_end(&mut buffer) {
			return Err(format!("{}", e))
		}
		
		let mut map = HashMap::new();
		let lines = buffer.split_by(10);
		'line_loop: for line in lines {
			let mut reading_key = false;
			let mut reading_val = false;
			let mut read_key = false;
			let mut key_bytes = Vec::new();
			let mut val_bytes = Vec::new();
			
			for byte in line {
				if !reading_key && !reading_val {
					if byte == 59 {
						continue 'line_loop;
					} else if byte != 32 {
						if read_key {
							reading_val = true;
							val_bytes.push(byte);
						} else {
							reading_key = true;
							key_bytes.push(byte);
						}
					}
				} else if reading_key {
					if byte == 61 {
						reading_key = false;
						read_key = true;
					} else if byte != 32 {
						key_bytes.push(byte);
					}
				} else if reading_val {
					if byte != 32 {
						val_bytes.push(byte);
					}
				}
			}
			
			if key_bytes.len() < 1 {
				continue;
			}
			
			let key = match String::from_utf8(decode(key_bytes)) {
				Ok(ok) => ok,
				Err(_) => continue
			}; let val = match String::from_utf8(decode(val_bytes)) {
				Ok(ok) => ok,
				Err(_) => continue
			}; map.insert(key, val);
		}

		Ok(Ini {
			map: map
		})
	}
	
	pub fn get(&mut self, key: &str) -> Option<String> {
		self.map.remove(key)
	}
	
	pub fn set(&mut self, key: &str, val: String) {
		self.map.insert(String::from(key), val);
	}
	
	pub fn save(self, path: String) -> Result<(), String> {
		let mut out = Vec::new();
		for (key, val) in self.map {
			out.append(&mut encode(key.into_bytes()));
			out.push(32);
			out.push(61);
			out.push(32);
			out.append(&mut encode(val.into_bytes()));
			out.push(10);
		}
		
		let mut handle = match File::create(path.as_str()) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		};
		
		if let Err(e) = handle.write(out.as_slice()) {
			return Err(format!("{}", e))
		} Ok(())
	}
}
		
