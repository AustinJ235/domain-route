use std::net::UdpSocket;
use std::process;
use dns_parser::*;
use std::process::{Command,Stdio};
use std::io::{Read,Write};
use std::fs::File;
use std::thread;
use std::sync::Arc;
use std::collections::HashMap;
use parking_lot::{Mutex,RwLock};
use std::time::{Instant,UNIX_EPOCH};
use misc::ini::Ini;
use std::net::SocketAddr;
use std::thread::JoinHandle;
use colored::*;
use bincode::{self,serialize, deserialize};

pub struct Server {
	handle: Option<JoinHandle<()>>,
	socket: Arc<UdpSocket>,
	gateway: String,
	dns: String,
	listen_bind: String,
	remote_bind: String,
	mode: Mode,
	domains: Vec<String>,
	routing_: Arc<Mutex<Vec<String>>>,
	cache_: Arc<RwLock<HashMap<Vec<u8>, CacheEntry>>>,
}

#[derive(PartialEq,Clone)]
enum Mode {
	Whitelist,
	Blacklist
}

#[derive(Serialize,Deserialize)]
pub struct CacheEntry {
	pub res: Vec<u8>,
	pub time: u64,
	pub ttl: u32,
}
	
pub fn now() -> u64 {
	match UNIX_EPOCH.elapsed() {
		Ok(ok) => ok.as_secs(),
		Err(_) => 0
	}
}

impl Server {
	fn save_cache(&self) -> Result<(), String> {
		let ref cache = *self.cache_.read();
		let bytes: Vec<u8> = match serialize(cache, bincode::SizeLimit::Infinite) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		};
		
		let mut handle = match File::create("cache.bin") {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		};
		
		if let Err(e) = handle.write_all(bytes.as_slice()) {
			return Err(format!("{}", e));
		} Ok(())
	}
	
	fn load_cache() -> Result<HashMap<Vec<u8>, CacheEntry>, String> {
		let mut bytes = Vec::new();
		let mut handle = match File::open("cache.bin") {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		};
		
		if let Err(e) = handle.read_to_end(&mut bytes) {
			return Err(format!("{}", e))
		}
	
		let decoded: HashMap<Vec<u8>, CacheEntry> = match deserialize(bytes.as_slice()) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		};
		
		Ok(decoded)
	}

	pub fn new() -> Result<Self, String> {
		let domains = {
			let mut out = Vec::new();
			let handle = match File::open("domains") {
				Ok(ok) => ok,
				Err(_) => return Err(format!("Failed to open 'domains'"))
			}; let mut line = Vec::new();
		
			for byte_ in handle.bytes() {
				if let Ok(byte) = byte_ {
					if byte == 10 {
						if line.len() > 0 {
							let line_string = match String::from_utf8(line.split_off(0)) {
								Ok(ok) => ok,
								Err(_) => {
									line = Vec::new();
									continue;
								}
							}; out.push(line_string.trim().to_owned());
						}
					} else {
						line.push(byte);
					}
				}
			} if line.len() > 0 {
				if let Ok(ok) = String::from_utf8(line) {
					out.push(ok.trim().to_owned());
				}
			}
		
			out
		};
	
		let mut ini = match Ini::load("config.ini".to_owned()) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("Failed to load config.ini, {}", e))
		}; let gateway = match ini.get("gateway") {
			Some(some) => some,
			None => return Err(format!("Ini 'gateway' isn't set."))
		}; let dns = match ini.get("dns") {
			Some(some) => some,
			None => return Err(format!("Ini 'dns' isn't set."))
		}; let listen_bind = match ini.get("listen_bind") {
			Some(some) => some,
			None => return Err(format!("Ini 'listen_bind' isn't set."))
		}; let remote_bind = match ini.get("remote_bind") {
			Some(some) => some,
			None => return Err(format!("Ini 'remote_bind isn't set."))
		};

		let mode = match ini.get("mode") {
			Some(some) => match some.as_str() {
				"whitelist" => Mode::Whitelist,
				"blacklist" => Mode::Blacklist,
				_ => return Err(format!("Ini 'mode' must be either 'whitelist' or 'blacklist'"))
			}, None => return Err(format!("Ini 'mode' isn't set."))
		};
		
		let socket = Arc::new(match UdpSocket::bind(listen_bind.as_str()) {
			Ok(ok) => ok,
			Err(e) => {
				println!("Failed to listen on {}, {}", listen_bind, e);
				process::exit(0);
			}
		});
		
		let cache_ = match Self::load_cache() {
			Ok(ok) => Arc::new(RwLock::new(ok)),
			Err(e) => {
				println!("Failed to load cache, {}", e);
				Arc::new(RwLock::new(HashMap::new()))
			}
		};
	
		Ok(Server {
			handle: None,
			socket: socket,
			gateway: gateway,
			dns: dns,
			listen_bind: listen_bind,
			remote_bind: remote_bind,
			mode: mode,
			domains: domains,
			routing_: Arc::new(Mutex::new(Vec::new())),
			cache_: cache_,
		})
	}

	pub fn socket(&self) -> Arc<UdpSocket> {
		self.socket.clone()
	}
	
	pub fn take_handle(&mut self) -> Option<JoinHandle<()>> {
		self.handle.take()
	}
	
	pub fn set_handle(&mut self, handle: JoinHandle<()>) {
		self.handle = Some(handle);
	}

	pub fn lookup(&self, use_port: u32, request: &Vec<u8>) -> Result<Vec<u8>, String> {
		let bind_to = format!("0.0.0.0:{}", use_port);
		let remote = match UdpSocket::bind(bind_to.as_str()) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		};

		if let Err(e) = remote.send_to(request.as_slice(), self.dns.as_str()) {
			return Err(format!("Failed to send, {}", e));
		}

		let mut buf = [0; 512];
		let len = match remote.recv_from(&mut buf) {
			Ok((len, _)) => len,
			Err(e) => return Err(format!("Failed to receive, {}", e))
		};
	
		let mut response = buf.to_vec();
		response.truncate(len);
	
		Ok(response)
	}
	
	fn add_route(&self, ip: &String) -> Result<(), String> {
		let child = match Command::new("sh")
			.arg("-c")
			.arg(format!("ip route add {} via {}", ip, self.gateway).as_str())
			.stdout(Stdio::null())
			.stderr(Stdio::piped())
			.spawn() {
				Ok(ok) => ok,
				Err(e) => return Err(format!("{}", e))
		};
		
		let out = match child.wait_with_output() {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		}; let stderr = match String::from_utf8(out.stderr) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("{}", e))
		};
		
		if stderr.len() > 0 {
			return Err(stderr);
		} Ok(())
	}
	
	pub fn remove(&self, rm_name: String) -> usize {
		let mut cache = self.cache_.write();
		let mut to_remove = Vec::new();
		
		for (req, _) in &*cache {
			let req_packet = match Packet::parse(req.as_slice()) {
				Ok(ok) => ok,
				Err(_) => continue
			};
			
			for question in req_packet.questions {
				let name = format!("{}", question.qname);
				
				if name == rm_name {
					to_remove.push(req.clone());
				}
			}
		}
		
		let mut removed = 0_usize;
		
		for remove in to_remove {
			if let Some(_) = cache.remove(&remove) {
				removed += 1;
			}
		}
		
		removed
	}
	
	pub fn handle(&self, id: usize, source: SocketAddr, request: Vec<u8>) {
		let mut route = false;
		let mut names = Vec::new();
		let req_packet = match Packet::parse(request.as_slice()) {
			Ok(ok) => ok,
			Err(e) => return println!("Failed to parse request packet, {}", e)
		};
		
		for question in req_packet.questions {
			let name = format!("{}", question.qname);
			for item in &self.domains {
				if (self.mode == Mode::Whitelist && name.contains(item)) || (self.mode == Mode::Blacklist && !name.contains(item)) {
					route = true;
				}
			} names.push(name);
		}
		
		if names.is_empty() {
			names.push(String::new());
		}
		
		let mut key = request.clone();
			key[0] = 0;
			key[1] = 0;	
			
		let mut cache_uped = false;
		let mut cache_used = false;
		let query_start = Instant::now();
		
		let (res, add_cache) = {
			if let Some(some) = self.cache_.read().get(&key) {
				if (some.ttl as u64) > now()-some.time {
					cache_uped = true;
					(match self.lookup(20000_u32 + id as u32, &request) {
						Ok(ok) => ok,
						Err(e) => return println!("Lookup failed, {}", e)
					}, true)
				} else {
					cache_used = true;
					let mut res = some.res.clone();
						res[0] = request[0];
						res[1] = request[1];
					(res, false)
				}
			} else {
				(match self.lookup(20000_u32 + id as u32, &request) {
					Ok(ok) => ok,
					Err(e) => return println!("Lookup failed, {}", e)
				}, true)
			}
		};
		
		let query_time = f32::floor(query_start.elapsed().subsec_nanos() as f32 / 1000000_f32) as u32;
		
		if let Err(e) = self.socket.send_to(res.as_slice(), source) {
			return println!("Failed to send response, {}", e)
		}
		
		let mut ttl = 0_u32;	
		{
			let res_packet = match Packet::parse(res.as_slice()) {
				Ok(ok) => ok,
				Err(e) => return println!("Failed to parse response packet, {}", e)
			};
			
			for answer in res_packet.answers {
				if answer.ttl < ttl || ttl == 0 {
					ttl = answer.ttl;
				}
		
				if route {
					if let RRData::A(addr) = answer.data {
						let ip = format!("{}", addr);
						let mut routing = self.routing_.lock();
				
						if !routing.contains(&ip) {
							if let Err(e) = self.add_route(&ip) {
								println!("{}", format!("Failed to add route, {}", e.trim()).red())
							} else {
								routing.push(ip);
							}
						}
					}
				}
			}
		}
		
		if add_cache {
			self.cache_.write().insert(key, CacheEntry {
				res: res,
				time: now(),
				ttl: ttl,
			});
		}
		
		let via = {
			if cache_used {
				String::from("cache")
			} else if cache_uped {
				String::from("update")
			} else {
				String::from("lookup")
			}
		};

		println!("{} for {} via {} in {}", format!("{}", source).green(), names[0].blue(), via.yellow(), format!("{} ms", query_time).red());
	}
	
	pub fn print_stats(&self) {
		let mut amt = 0_usize;
		let cache = self.cache_.read();

		for (_, _) in &*cache {
			amt += 1;
		}

		println!("{}", format!("{} cache entries.", amt).magenta());
	}
		
	pub fn start(self) -> Arc<RwLock<Self>> {
		let srv_ = Arc::new(RwLock::new(self));
		let _srv_ = srv_.clone();
		
		let handle = thread::spawn(move || {
			let mut buf = [0; 512];
			let mut id_counter = 0_usize;
			let mut since_stats = 0_usize;
			let socket = _srv_.read().socket();
	
			loop {
				since_stats += 1;
		
				if since_stats > 9 {
					since_stats = 0;
					let srv = _srv_.read();
					srv.print_stats();
					
					if let Err(e) = srv.save_cache() {
						println!("Failed to save cache, {}", e);
					}
				}
			
				if let Ok((len, source)) = socket.recv_from(&mut buf) {
					let mut request = buf.to_vec();
					request.truncate(len);
				
					id_counter += 1;
					if id_counter >= 1000 {
						id_counter = 0;
					} let id = id_counter.clone();
					
					let __srv_ = _srv_.clone();
					
					thread::spawn(move || {
						let srv = __srv_.read();
						srv.handle(id, source, request);
					});
				}
			}
		});
		
		srv_.write().set_handle(handle);
		srv_			
	}
}

pub trait Stop {
	fn wait_stop(&self);
	fn stop(&self);
}

impl Stop for Arc<RwLock<Server>> {
	fn wait_stop(&self) {
		let handle_ = self.write().take_handle();
		if let Some(handle) = handle_ {
			let _ = handle.join();
		}
	} fn stop(&self) {
	
	}
}
		
