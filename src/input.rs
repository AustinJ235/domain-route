use std::io;
use std::thread::{self,JoinHandle};
use parking_lot::{RwLock};
use std::sync::Arc;
use server::Server;
use colored::*;

pub struct Input {
	handle: Option<JoinHandle<()>>,
	server_: Arc<RwLock<Server>>,
}

impl Input {
	pub fn new(server_: Arc<RwLock<Server>>) -> Result<Self, String> {
		Ok(Input {
			handle: None,
			server_: server_,
		})
	}
	
	pub fn set_handle(&mut self, handle: JoinHandle<()>) {
		self.handle = Some(handle);
	}
	
	pub fn start(self) -> Arc<RwLock<Self>> {
		let input_ = Arc::new(RwLock::new(self));
		let _input_ = input_.clone();
		
		let handle = thread::spawn(move || {
			'main: loop {
				let mut line = String::new();
				match io::stdin().read_line(&mut line) {
					Ok(_) => {
						let mut line_bytes = line.into_bytes();

						for i in 0..line_bytes.len() {
							if line_bytes[i] == 32 {
								let split_at = i+1;	
							
								if split_at >= line_bytes.len() {
									line_bytes.pop();
									
									let cmd = match String::from_utf8(line_bytes) {
										Ok(ok) => ok,
										Err(_) => continue 'main
									}; let args = String::new();
									
									input_.read().handle(cmd, args);
									continue 'main;
								} else {
									if line_bytes[split_at] == 32 {
										continue;
									}
								
									let mut args_bytes = line_bytes.split_off(split_at);
									args_bytes.pop();
									line_bytes.pop();
									
									let cmd = match String::from_utf8(line_bytes) {
										Ok(ok) => ok,
										Err(_) => continue 'main
									}; let args = match String::from_utf8(args_bytes) {
										Ok(ok) => ok,
										Err(_) => continue 'main
									};
									
									input_.read().handle(cmd, args);
									continue 'main;
								}
							}
						}
						
						let cmd = match String::from_utf8(line_bytes) {
							Ok(ok) => ok,
							Err(_) => continue 'main
						}; let args = String::new();
						
						input_.read().handle(cmd, args);
					}, Err(e) => {
						println!("Failed to read line from stdin, {}", e);
					}
				}
			}		
		});
		
		_input_.write().set_handle(handle);
		_input_
	}
	
	pub fn handle(&self, cmd: String, args: String) {
		match cmd.as_str() {
			"remove" => {
				let removed = self.server_.read().remove(args);
				println!("{}", format!("Removed {} cache entries.", removed).green());
			}, _ => {
				println!("{}", "Command not found!".red());
			}
		}
	}
}
