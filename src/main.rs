extern crate dns_parser;
extern crate parking_lot;
extern crate colored;
extern crate serde;
extern crate bincode;
#[macro_use]
extern crate serde_derive;

pub mod misc;
pub mod server;
pub mod input;

use server::{Server,Stop};
use input::Input;
			
fn main() {
	let srv = Server::new().unwrap();
	let srv_ = srv.start();
	
	let input = Input::new(srv_.clone()).unwrap();
	let input_ = input.start();
	
	srv_.wait_stop();
}

